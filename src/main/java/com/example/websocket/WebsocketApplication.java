package com.example.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class WebsocketApplication {
	private Logger logger = LoggerFactory.getLogger(WebsocketApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(WebsocketApplication.class, args);
	}

	int info = 1;

	@Scheduled(fixedRate = 1000)
	public void outputLogger() {
		logger.info("测试日志输出" + info++);
		throw new RuntimeException();
	}
}
