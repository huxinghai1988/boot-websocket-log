### USING
  My Hello World!!

### INSTALL

  brew install myi

### Hello World!


```java
public class DataModel implements Serializable {
    public static final long serialVersionUID = 1L;
    public int num;
    //...
}

//序列化过程
DataModel dm = new DataModel();
//...setData
ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("a.txt"));
out.writeObject(dm);
out.close();

//反序列化过程
ObjectInputStream in = new ObjectInputStream(new FileInputStream("a.txt"));
DataModel newDm = (DataModel) in.readObject();
in.close();
```

``` javascript
import GiteeTrending from '../../../lib/GiteeTrending'

let users = [
  {
    id: 1,
    name: 'hello'
  }, {
    id: 2,
    name: 'world'
  }
]

// 新建 GiteeTrending 对象实例，并设置标识名，这个标识名用于区分历史记录
let trendingUsers = new GiteeTrending('user')
let trendingProjects = new GiteeTrending('project')

// 对 id 为 1 的用户的排序权重 + 1
trendingUsers.add(1)

// 对 users 进行排序
users = trendingUsers.sort(users)
```

```XML
<plugin>
  <groupId>org.apache.maven.plugins</groupId>
  <artifactId>maven-javadoc-plugin</artifactId>
  <version>3.0.0</version>
  <configuration>
    ...
  </configuration>
</plugin>
```